# OpenML dataset: Goodreads-Computer-Books

https://www.openml.org/d/43785

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The reason for creating this dataset is the requirement of a good clean dataset of computer books. I had searched for datasets on books in Kaggle and I found out that while most of the datasets had a good amount of books listed, there were either major columns missing or grossly unclean data. I mean, you can't determine how good a book is just from a few text reviews. So I collected this data from the Goodreads website from the "Computer" category to help people who are like this type of book.
Acknowledgements
This data was entirely scraped via the Webdriver
Inspiration
The reason behind creating this dataset is pretty straightforward, I'm listing the books for all who need computer books, irrespective of the language and publication and all of that. So go ahead and use it to your liking, find out what book you should be reading next,  all possible approaches to exploring this dataset are welcome.
I started creating this dataset on Jan 18, 2021, and intend to update it frequently.
P.S. If you like this, please don't forget to give an upvote!
Notes
The missing values are imputed in this data by the creator.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43785) of an [OpenML dataset](https://www.openml.org/d/43785). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43785/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43785/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43785/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

